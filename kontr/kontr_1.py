import math

a = 5
x = 10

def G(x):
    return (- 2 * (7 * a**2 - 17 * a * x + 6 * x**2))/(21 * a**2 + a * x - 36 * x**2)

def rectangle_rule(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.0
    xstart = a + dx
    for i in range(nseg):
        sum += func(xstart + i * dx)
    return sum * dx

def trapezium(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.5 * (func(a) + func(b))
    for i in range(1, nseg):
        sum += func(a + i * dx)
    return sum * dx

def simpson(f, a, b, n):
    h = (b-a)/n
    k = 0.0
    x = a + h
    for i in range(1, n//2 + 1):
        k += 4*f(x)
        x += 2*h
    x = a + 2*h
    for i in range(1, n//2):
        k += 2*f(x)
        x += 2*h
    return (h/3)*(f(a)+f(b)+k)

print('rectangle', rectangle_rule(G, 1, 10, 5))
print('trapezium', trapezium(G, 1, 10, 5))
print('simpson', simpson(G, 1, 10, 5))
