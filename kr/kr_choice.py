import kr_sorts

lst = list(map(int, input('Введите числа, который нужно отсортировать через пробел: ').split()))
choice_one = int(input('Выберите алгоритм сортировки: 1 - быстрая, 2 - вставками, 3 - пирамидальная, 4 - тимсорт: '))
choice_two = int(input('Выберите сортировку по: 1 - возрастанию, 2 - убыванию: '))

if choice_one == 1 and choice_two == 1:
    print(f'Результат быстрой сортировки: {kr_sorts.qs(lst)}')
if choice_one == 1 and choice_two == 2:
    print(f'Результат обратной быстрой сортировки: {kr_sorts.qs(lst, reverse=True)}')
if choice_one == 2 and choice_two == 1:
    print(f'Результат сортировки вставками: {kr_sorts.ins_sort(lst)}')
if choice_one == 2 and choice_two == 2:
    print(f'Результат обратной сортировки вставками: {kr_sorts.ins_sort(lst, reverse=True)}')
if choice_one == 3 and choice_two == 1:
    print(f'Результат пирамидальной сортировки: {kr_sorts.heap_sort(lst)}')
if choice_one == 3 and choice_two == 2:
    print(f'Результат пирамидальной сортировки: {kr_sorts.heap_sort(lst, reverse=True)}')
if choice_one == 4 and choice_two == 1:
    print(f'Результат тимсорта : {kr_sorts.tim_sort(lst)}')
if choice_one == 4 and choice_two == 2:
    print(f'Результат обратного тимсорта: {kr_sorts.tim_sort(lst, reverse=True)}')
