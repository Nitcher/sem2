import networkx as nx
import lab_3_lib

def test_shortest_path():
    graph_data = lab_3_lib.graph_loader('data.txt')
    nx_graph = lab_3_lib.get_nx_graph(graph_data)
    my_graph = lab_3_lib.get_my_graph(graph_data[0], graph_data[1])
    expected_path = nx.shortest_path(nx_graph, source='A', target='D')
    my_path = min([(len(e), e) for e in lab_3_lib.find_all_paths(my_graph, 'A', 'D')])[1]
    assert my_path == expected_path
