import cProfile
import pstats
import networkx as nx
from matplotlib import pyplot as plt
from lab_3_lib import get_my_graph, find_all_paths

def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('flag') != -1:
            result = line.split('   ')[0]
            break
    return result

min_size = 1
max_size = 10
step = 1

array_len = [i for i in range(min_size, max_size, step)]
operations = []

for i in range(min_size, max_size, step):
    G = nx.complete_graph(i)
    graph = get_my_graph(list(G.nodes()), list(G.edges()))
    cProfile.run('find_all_paths(graph, 0, i)', 'stats.log')
    with open('output.txt', 'w') as log_file:
        p = pstats.Stats('stats.log', stream=log_file)
        p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operations.append(int(line))
    print(i)

plt.style.use('grayscale')
fig = plt.figure()
ax = fig.add_subplot()

ax.plot(array_len, operations, linewidth=3, c='red', label='Практическая сложность')
ax.set_title('Сложность алгоритма поиска кратчайшего пути', fontsize=11, c='black')
ax.set_xlabel('Размер массива, элементов', fontsize=14, c='black')
ax.set_ylabel('Кол-во операций, шт')

ax.grid('black')
ax.legend(prop={'size':10})
plt.show()