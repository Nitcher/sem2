#quick_sort

def qs(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    elem = lst[0]
    left = list(filter(lambda x: x < elem, lst))
    center = list(filter(lambda x: x == elem, lst))
    right = list(filter(lambda x: x > elem, lst))
    if not reverse:
        return qs(left) + center + qs(right)
    elif reverse:
        return list(reversed(qs(left) + center + qs(right)))

#insertion sort

def ins_sort(lst, reverse=False):
    for i in range(1, len(lst)):
        num_now = lst[i]
        pos = i
        while pos > 0 and lst[pos - 1] > num_now:
            lst[pos] = lst[pos - 1]
            pos -= 1
        lst[pos] = num_now
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))

#heapsort

def heapify(lst, heap_size, root_index):
    largest = root_index
    left_child = (2 * root_index) + 1
    right_child = (2 * root_index) + 2
    if left_child < heap_size and lst[left_child] > lst[largest]:
        largest = left_child
    if right_child < heap_size and lst[right_child] > lst[largest]:
        largest = right_child
    if largest != root_index:
        lst[root_index], lst[largest] = lst[largest], lst[root_index]
        heapify(lst, heap_size, largest)

def heap_sort(lst, reverse=False):
    n = len(lst)
    for i in range(n, -1, -1):
        heapify(lst, n, i)
    for i in range(n - 1, 0, -1):
        lst[i], lst[0] = lst[0], lst[i]
        heapify(lst, i, 0)
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))
#timsort

MINIMUM = 32

def find_minrun(n):
    r = 0
    while n >= MINIMUM:
        r |= n & 1
        n >>= 1
    return n + r

def insertion_sort(lst, left, right):
    for i in range(left + 1, right + 1):
        element = lst[i]
        j = i - 1
        while element < lst[j] and j >= left:
            lst[j + 1] = lst[j]
            j -= 1
        lst[j + 1] = element
    return lst

def merge(lst, l, m, r):
    lst_length1 = m - l + 1
    lst_length2 = r - m
    left = []
    right = []
    for i in range(0, lst_length1):
        left.append(lst[l + i])
    for i in range(0, lst_length2):
        right.append(lst[m + 1 + i])

    i = 0
    j = 0
    k = l

    while j < lst_length2 and i < lst_length1:
        if left[i] <= right[j]:
            lst[k] = left[i]
            i += 1

        else:
            lst[k] = right[j]
            j += 1

        k += 1

    while i < lst_length1:
        lst[k] = left[i]
        k += 1
        i += 1

    while j < lst_length2:
        lst[k] = right[j]
        k += 1
        j += 1

def tim_sort(lst, reverse=False):
    n = len(lst)
    minrun = find_minrun(n)

    for start in range(0, n, minrun):
        end = min(start + minrun - 1, n - 1)
        insertion_sort(lst, start, end)

    size = minrun
    while size < n:

        for left in range(0, n, 2 * size):
            mid = min(n - 1, left + size - 1)
            right = min((left + 2 * size - 1), (n - 1))
            merge(lst, left, mid, right)

        size = 2 * size
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))