from kontr_1 import rectangle_rule, trapezium, simpson, G
import matplotlib.pyplot as plt

a = 0.1
x = 10

integrated_values = ([], [], [], [], [])
inter = []

for i in range(10, 100, 10):
    integrated_values[0].append(rectangle_rule(G, -3, 3, i))
    integrated_values[1].append(trapezium(G, -3, 3, i))
    integrated_values[2].append(simpson(G, -3, 3, i))
    inter.append(i)

for i in range(3):
    for j in range(len(integrated_values[i])):
        integrated_values[i][j] = (0.05 - integrated_values[i][j])

plt.plot(inter, integrated_values[0], label="RectanglesRule")
plt.plot(inter, integrated_values[1], 'y', label="TrapeziumRule")
plt.plot(inter, integrated_values[2], 'm', label="SimpsonRule")
plt.xlabel("Количество точек разделения")
plt.ylabel("Абсолютная точность")
plt.legend()
plt.show()
