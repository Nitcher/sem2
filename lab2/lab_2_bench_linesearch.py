import cProfile
import pstats
from lab_2_searchs import linesearch
from pstats import SortKey
import matplotlib.pyplot as plt
from random import uniform

def n_complexity(array):
    return [e for e in array]

def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('flag') != -1:
            result = line.split('   ')[0]
            break
    return result

operation_lst = []
min_size = 10000
max_size = 100000
step = 10000
operation_count = []
for i in range(min_size, max_size, step):
    lst = []
    for x in range(i):
        lst.append(uniform(-100, 100))
    elem = lst[-42]
    operation_count.append(len(lst))
    cProfile.run('linesearch(lst, elem)', 'stats.log')
    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operation_lst.append(int(line))
operation_lst_c = [i for i in operation_lst]

array = [i for i in range(min_size, max_size, step)]

plt.style.use('grayscale')
fig = plt.figure()
ax = fig.add_subplot()

ax.plot(operation_count, n_complexity(array), linewidth=3, linestyle='--', label='Теоретическая сложность')
ax.plot(operation_count, operation_lst_c, linewidth=3, c='red', label='Линейный поиск')
ax.set_title('Зависимость количества операций от размера массива', fontsize=11, c='black')
ax.set_xlabel('Размер массива, элементов', fontsize=14, c='black')
ax.set_ylabel('Кол-во операций, шт')

ax.grid('black')
ax.legend(prop={'size':10})
plt.show()