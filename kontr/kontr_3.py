from random import random
import numpy as np
import matplotlib.pyplot as plt

size = list(np.linspace(10, 200000, 1000))
pi = []
pi_line = [3.14 for i in range(len(size))]

def mc_multiple_runs(size, hits=0):
    for i in range(int(size)):
        x, y = random(), random()
        if x ** 2 + y ** 2 < 1:
            hits = hits + 1
    return float(hits)

for i in size:
    pi.append(4 * (mc_multiple_runs(i) / i))
    print('hits : %d, trials: %d, estimate pi = %1.4F' % (mc_multiple_runs(i), i, 4 * (mc_multiple_runs(i) / i)))

fig, ax = plt.subplots()

ax.plot(size, pi, 'brown')
ax.plot(size, pi_line, 'y')
plt.title('Оценка значения Pi методом Монте-Карло')
plt.xlabel('Кол-во испытания')
plt.ylabel('Расчетное значение Pi')
plt.ylim(3.11, 3.17)
plt.show()

