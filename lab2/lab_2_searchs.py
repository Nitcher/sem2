def flag():
    return None

def prefix(x):
    d = {0: 0}
    for i in range(1, len(x)):
        j = d[i-1]
        while j > 0 and x[j] != x[i]:
            j = d[j-1]
        if x[j] == x[i]:
            j += 1
        d[i] = j
    return d

def linesearch(lst, elem):
    for i in range(len(lst)):
        flag()
        if lst[i] == elem:
            return i
    return -1

def binarysearch(lst, elem):
    first = 0
    last = len(lst) - 1
    index = -1
    while (first <= last) and (index == -1):
        flag()
        mid = (first + last) // 2
        if lst[mid] == elem:
            index = mid
        else:
            if elem < lst[mid]:
                last = mid - 1
            else:
                first = mid + 1
    return index

def naivesearch(txt, fragm):
    len_fragm = len(fragm)
    len_txt = len(txt)
    for i in range(len_txt-len_fragm+1):
        status = 1
        for j in range(len_fragm):
            flag()
            if txt[i+j] != fragm[j]:
                status = 0
                break
        if j == len_fragm-1 and status != 0:
            return i

def kmp(txt, fragm):
    d = prefix(fragm)
    i = j = 0
    while i < len(txt) and j < len(fragm):
        flag()
        if fragm[j] == txt[i]:
            i += 1
            j += 1
        elif j == 0:
            i += 1
        else:
            j = d[j-1]
    else:
        if j == len(fragm):
            return i-j
        return None
