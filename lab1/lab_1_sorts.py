def swap():
    return None



def bubble_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swapped = True
        while swapped:
            swapped = False
            for i in range(len(lst) - 1):
                swap()
                if lst[i] > lst[i + 1]:
                    lst[i], lst[i + 1] = lst[i + 1], lst[i]
                    swapped = True
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def shell_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        t = len(lst) // 2
        while t > 0:
            for i in range(len(lst) - t):
                j = i
                swap()
                while j >= 0 and lst[j] > lst[j + t]:
                    lst[j], lst[j + t] = lst[j + t], lst[j]
                    j -= 1
            t = t // 2
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def ins_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        for i in range(1, len(lst)):
            key = lst[i]
            j = i - 1
            while j >= 0 and lst[j] > key:
                swap()
                lst[j + 1] = lst[j]
                j -= 1
            lst[j + 1] = key
        if not reverse:
            return lst
        elif reverse:
            return list(reversed(lst))


def qs(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    elem = lst[0]
    left = list(filter(lambda x: x < elem, lst))
    center = list(filter(lambda x: x == elem, lst))
    right = list(filter(lambda x: x > elem, lst))
    if not reverse:
        return qs(left) + center + qs(right)
    elif reverse:
        return list(reversed(qs(left) + center + qs(right)))