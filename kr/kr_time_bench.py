from random import randint
import matplotlib.pyplot as plt
from kr_sorts import qs, heap_sort, tim_sort, ins_sort
from timeit import default_timer
import sys

sys.setrecursionlimit(20000)

def elapsed_time(function, array: list, repeats=1):
    result = 0
    for i in range(repeats):
        start_clock = default_timer()
        function(array)
        end_clock = default_timer() - start_clock
        result += end_clock
    return result/repeats

step = 100
max_size = 10000
min_size = 100

sort_names = [[], [], [], []]
sort_names_count = []

for j in range(min_size, max_size, step):
    lst = []
    for x in range(j):
        lst.append(randint(-10000, 10000))
    sort_names_count.append(len(lst))
    sort_names[0].append(elapsed_time(qs, lst))
    sort_names[1].append(elapsed_time(ins_sort, lst))
    sort_names[2].append(elapsed_time(heap_sort, lst))
    sort_names[3].append(elapsed_time(tim_sort, lst))

plt.style.use('grayscale')
fig = plt.figure()
fig.text(0.01, 0.5, 'Время выполнения, с', va='center', rotation='vertical', size=16)

ax_1 = fig.add_subplot(2, 1, 1)
ax_2 = fig.add_subplot(2, 1, 2)

ax_1.plot(sort_names_count, sort_names[1], linewidth=3, c='red', label='Сортировка вставками')
ax_1.set_title('Зависимость времени от размера массива', fontsize=18, c='black')
ax_1.set_xlabel('Размер массива, элементов', fontsize=14, c='black')
ax_1.set_ylabel('   ')

ax_1.grid('black')
ax_1.legend(prop={'size':10})

ax_2.plot(sort_names_count, sort_names[0], linewidth=3, c='purple', label='Быстрая сортировка')
ax_2.plot(sort_names_count, sort_names[2], linewidth=3, c='red', linestyle='-.', label='Пирамидальная сортировка')
ax_2.plot(sort_names_count, sort_names[3], linewidth=3, c='yellow', label='Тимсорт')
ax_2.set_xlabel('Размер массива, элементов', fontsize=14, c='black')
ax_2.set_ylabel('   ')

ax_2.grid('black')
ax_2.legend(prop={'size':7})

plt.show()