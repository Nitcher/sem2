from numpy import exp, linspace, convolve
import statistics
import copy
import collections
import xlrd
import matplotlib.pyplot as plt

def median_filter(data, window_size):
    window = []
    output = []
    for elem in data:
        window.append(elem)
        if len(window) > window_size:
            temp = []
            for i in range(len(window)):
                if i == 0:
                    pass
                else:
                    temp.append(window[i])
            window = temp
        if len(window) == 0:
            output = 0
        else:
            output.append(statistics.median(window))
    return output

def sma(data, window):
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:
            queue.popleft()
            queue_length -= 1
        if queue_length == 0:
            lst[i] = 0
        else:
            lst[i] = sum(queue) / queue_length
    return lst

def ema(data, window):
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue_length = len(queue)
        if queue_length == 0:
            queue.append(lst[i])
            pass
        elif queue_length > window:
            queue.popleft()
        alpha = 2 / (window + 1)
        lst[i] = (alpha * lst[i]) + ((1 - alpha) * queue[-1])
        queue.append(lst[i])
    return lst

file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(12, start_rowx=1, end_rowx=5002)
data = [float(elem) for elem in data]

print(data)
print(len(data))


mf = median_filter(data, 25)
sma = sma(data, 25)
ema = ema(data, 25)
size = [i for i in range(len(data))]


plt.title('График отфильтрованных и изначальных данных')
plt.xlabel('Номер элемента')
plt.ylabel('Значение')
plt.plot(size, data)
plt.plot(size, sma, 'red', label="SMA")
plt.plot(size, mf, 'green', label="MF")
plt.plot(size, ema, 'blue', label="EMA")
plt.xlim(0, 400)
plt.legend()
plt.show()
