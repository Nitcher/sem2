import cProfile
import pstats
from pstats import SortKey
import matplotlib.pyplot as plt
from random import randint
from lab_1_sorts import qs
import sys
from math import log

sys.setrecursionlimit(20000)

def n_log_n_complexity(array):
    return [e*log(e, 2) for e in array]

def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('lambda') != -1:
            result = line.split('   ')[0]
            break
    return result

operation_lst = []
min_size = 100
max_size = 10000
step = 1000
operation_count = []
for i in range(min_size, max_size, step):
    lst = []
    for x in range(i):
        lst.append(randint(-10000, 10000))
    operation_count.append(len(lst))
    cProfile.run('qs(lst)', 'stats.log')
    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operation_lst.append(int(line))
operation_lst_c = [i for i in operation_lst]

array = [i for i in range(min_size, max_size, step)]

plt.style.use('grayscale')
fig = plt.figure()
ax = fig.add_subplot()

ax.plot(operation_count, n_log_n_complexity(array), linewidth=3, linestyle='--', label='Теоретическая сложность')
ax.plot(operation_count, operation_lst_c, linewidth=3, c='red', label='Быстрая сортировка')
ax.set_title('Зависимость количества операций от размера массива', fontsize=11, c='black')
ax.set_xlabel('Размер массива, элементов', fontsize=14, c='black')
ax.set_ylabel('Кол-во операций, шт')

ax.grid('black')
ax.legend(prop={'size':10})
plt.show()