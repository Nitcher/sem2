import pytest
from lab_2_searchs import linesearch, binarysearch, naivesearch, kmp
import pytest

elem_num = 5
elem_str = 'sf'
lst_num = [9, 4, 2, 4, 5, 6, 4, 10, 0, 12]
lst_str = 'slfksflkgs'
lst_input = [1, 2, 3, 'fsasfasf']

def test_search():
    practice = [[], [], [], []]
    theory = 4
    practice[0] = linesearch(lst_num, elem_num)
    practice[1] = binarysearch(lst_num, elem_num)
    practice[2] = naivesearch(lst_str, elem_str)
    practice[3] = kmp(lst_str, elem_str)
    assert theory == practice[0]
    assert theory == practice[1]
    assert theory == practice[2]
    assert theory == practice[3]

def test_input_data():
    with pytest.raises(TypeError):
        list(linesearch(lst_input, elem_num))
    with pytest.raises(TypeError):
        list(binarysearch(lst_input, elem_num))
    with pytest.raises(TypeError):
        list(naivesearch(lst_input, elem_str))
    with pytest.raises(TypeError):
        list(kmp(lst_input, elem_str))