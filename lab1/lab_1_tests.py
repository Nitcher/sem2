from lab_1_sorts import bubble_sort, ins_sort, shell_sort, qs
from random import randint
import pytest

def test_ascending():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice = [[], [], [], []]
    practice[0] = bubble_sort(lst)
    practice[1] = ins_sort(lst)
    practice[2] = shell_sort(lst)
    practice[3] = qs(lst)
    assert theory == list(practice[0])
    assert theory == list(practice[1])
    assert theory == list(practice[2])
    assert theory == list(practice[3])

def test_descending():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice = [[], [], [], []]
    practice[0] = bubble_sort(lst, reverse=True)
    practice[1] = ins_sort(lst, reverse=True)
    practice[2] = shell_sort(lst, reverse=True)
    practice[3] = qs(lst, reverse=True)
    assert theory == list(practice[0])
    assert theory == list(practice[1])
    assert theory == list(practice[2])
    assert theory == list(practice[3])

def test_input_data():
    lst = [1, 'fkak', 10, 5.2442, '95,51']
    with pytest.raises(TypeError):
        list(bubble_sort(lst))
    with pytest.raises(TypeError):
        list(ins_sort(lst))
    with pytest.raises(TypeError):
        list(shell_sort(lst))
    with pytest.raises(TypeError):
        list(qs(lst))

def test_stability_ascending():
    lst = [(1, 1), (2, 3), (4, 0), (5, 5), (5, 9), (7, 0), (10, 12), (7, 8)]
    theory = sorted(lst)
    practice = [[], [], [], []]
    practice[0] = bubble_sort(lst)
    practice[1] = ins_sort(lst)
    practice[2] = shell_sort(lst)
    practice[3] = qs(lst)
    assert theory == list(practice[0])
    assert theory == list(practice[1])
    assert theory == list(practice[2])
    assert theory == list(practice[3])

def test_stability_descending():
    lst = [(1, 1), (2, 3), (4, 0), (5, 5), (5, 9), (7, 0), (10, 12), (7, 8)]
    theory = list(reversed(sorted(lst)))
    practice = [[], [], [], []]
    practice[0] = bubble_sort(lst, reverse=True)
    practice[1] = ins_sort(lst, reverse=True)
    practice[2] = shell_sort(lst, reverse=True)
    practice[3] = qs(lst, reverse=True)
    assert theory == list(practice[0])
    assert theory == list(practice[1])
    assert theory == list(practice[2])
    assert theory == list(practice[3])